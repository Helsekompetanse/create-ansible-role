#!/usr/bin/env python
from __future__ import unicode_literals, absolute_import, print_function

import os
import shutil
from collections import OrderedDict
from cookiecutter.prompt import read_user_yes_no

try:
    input = raw_input
except NameError:
    pass


folders = OrderedDict()

folders['tasks']= {
    'question': '\nShould it have tasks? ',
    'hint': '  Add task file name i.e (install_nginx.yml) ',
    'action': '- include_tasks: "{}"\n',
    'create_a_yaml_file': True
}

folders['handlers'] = {
    'question': '\nShould it have handlers?',
    'hint': '  Add handler name i.e (restart nginx) ',
    'action': '- name: {}\n  # TODO\n\n'
}

folders['defaults'] = {
    'question': '\nIt should contain default variables? ',
    'hint': '  Add variable i.e (some_variable: "value") ',
    'action': '{}\n\n'
}

folders['meta']= {
    'question': '\nShould it have meta info? ',
    'pre_hint': ' - Should it have dependencies? ',
    'pre_action': '\ndependencies:\n',
    'hint': '    Add dependency i.e ({role: database, port: "1234"}) ',
    'action': '  - {}\n'
}

folders['templates'] = {
    'question': '\nShould it have templates? ',
}

folders['files'] = {
    'question': '\nShould it have files? ',
}


def install_yaml_template(folder_name, file_name='main.yml'):
    destination = os.path.join(folder_name, file_name)
    shutil.copyfile('cookiecutter_yaml_template.yml', destination)

def remove_yaml_template():
    os.remove('cookiecutter_yaml_template.yml')

def configure_role():
    print('\n\nROLE CONFIGURATION:\n===================')
    for folder_name, folder_conf in folders.items():
        if read_user_yes_no(folder_conf['question'], default_value=u'yes'):

            try:
                # this file has to be there, git doesn't store empty folders.
                os.remove(os.path.join(folder_name, '.empty'))
            except OSError:
                pass

            if 'hint' in folder_conf:
                install_yaml_template(folder_name)
                with open(os.path.join(folder_name, 'main.yml'), 'a') as fp:

                    if 'pre_hint' in folder_conf:
                        if read_user_yes_no(folder_conf['pre_hint'], \
                                            default_value=u'yes'):
                            fp.write(folder_conf['pre_action'])
                        else:
                            continue

                    action_name = input(folder_conf['hint'])
                    while action_name:
                        fp.write(folder_conf['action'].format(action_name))
                        if 'create_a_yaml_file' in folder_conf \
                                and folder_conf['create_a_yaml_file']:
                            install_yaml_template(folder_name, action_name)
                        action_name = input(folder_conf['hint'])
        else:
           shutil.rmtree(folder_name)
    remove_yaml_template()


if __name__ == '__main__':
    configure_role()
