# A Cookiecutter: Create Ansible Role #

**This project is a fork of [iknite's cookiecutter-ansible-role](https://github.com/iknite/cookiecutter-ansible-role)**, which is © Enrique Paredes with a 3-clause BSD license. You might want to check out the documentation in the original project as well. The main difference is that the original project creates [Ansible Galaxy](https://galaxy.ansible.com/) compatible roles, which isn't very helpful unless you are publishing roles to [Ansible Galaxy](https://galaxy.ansible.com/) at a regular basis.

## Getting started ###

Install a recent [cookiecutter](https://github.com/audreyr/cookiecutter).


```
#!bash

$ sudo pip install cookiecutter --upgrade pip # Installs a recent cookiecutter system wide.
```

Install the _cookiecutter_.

```
#!bash

$ git clone https://bitbucket.org/Helsekompetanse/create-ansible-role.git $HOME/.cookiecutters/create-ansible-role
```

Go to your Ansible roles folder and start adding new roles.

```
#!bash

$ cd /some/path/example/provisioning/roles
$ cookiecutter create-ansible-role
```

Follow the prompts.